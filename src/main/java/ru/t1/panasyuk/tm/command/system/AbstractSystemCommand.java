package ru.t1.panasyuk.tm.command.system;

import ru.t1.panasyuk.tm.api.service.ICommandService;
import ru.t1.panasyuk.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

}